# README #

Useful when taking 100s/1000s of entries from Nessus scan and condensing to one detail item. The script is used to take a Nessus file containing only vulnerabilities that are of interest and output all CVE's to an HTML output containing links.

### Output Example ###

![Mozilla Firefox 2015-04-19 19-24-57.jpg](https://bitbucket.org/repo/jy8z5a/images/2365281800-Mozilla%20Firefox%202015-04-19%2019-24-57.jpg)

### How do I get set up? ###

* Download Script
* install using easy_install
* Run script passing the arguments needed

### Note ###

MAC

1.Mac and Unix users can simply use the following command;

>sudo curl https://bootstrap.pypa.io/ez_setup.py -o - | python

2.Once the setuptools install is completed download the install package from [here](https://bitbucket.org/laned/cvewriter/downloads/CveWriter-1.0.0.tar.gz) then you are able to run the setup script using easy_install;

>sudo easy_install CveWriter-1.0.0.tar.gz <----(The file you just downloaded from the link in step 2)

3.You can now execute the script from any terminal (you may need to close a currently open terminal and reopen it)


### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact