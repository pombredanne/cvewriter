# README #

Useful when taking 100s/1000s of entries from Nessus scan and condensing to one detail item. Used to take a list of CVEs and create an HTML file containing links to each CVE ID for your report.

### Output Example ###

![Mozilla Firefox 2015-04-19 19-24-57.jpg](https://bitbucket.org/repo/jy8z5a/images/2365281800-Mozilla%20Firefox%202015-04-19%2019-24-57.jpg)

### How do I get set up? ###

* Download Script
* install using easy_install
* Run script passing the arguments needed

### Note ###

MAC

1.Mac and Unix users can simply use the following command;

>sudo curl https://bootstrap.pypa.io/ez_setup.py -o - | python

2.Once the setuptools install is completed you are able to run teh setup script using easy_install;

>sudo easy_install tool-VERSION.tar.gz

3.You can now execute the script from any terminal (you may need to close a currently open terminal and reopen it)


### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact